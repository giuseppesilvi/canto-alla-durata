%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../099-articolo-musica-tecnologia.tex
%
%-------------------------------------------------------------------------------
%--------------------------------------------------------------------- SECTION -
%-------------------------------------------------------------------------------
\begin{figure}[ht]
  \begin{addmargin*}[0pt]{-\marginparsep-\marginparwidth}
    \centering
    \includegraphics[width=\linewidth]{images/lazzaro-artescienza-luglio-20231323.jpg}
    %\captionsetup{width=.81\linewidth}
    %\caption{Catalogo della IV posizione}
  \end{addmargin*}
  \label{cat:IV}
\end{figure}
%
\marginpar{%
  \captionof{figure}{\raggedright Accordatura notturna prima del concerto,
  Goethe Institut Rom, con ascoltatori di prestigio. Un sistema basato su
  feedback elettroacustico risente dell'acustica della sala e delle condizioni
  ambientali. Il desiderio di portare lo strumento in concerto deriva anche da
  semplici piccoli problemi come questo. E su questi problemi è facile misurare
  l'avanzamento del progetto e la solidità della struttura perché, per esempio,
  tra il primo concerto LAZZARO (07 luglio 2023) e l'ultimo (22 marzo 24) lo
  strumento è passato da necessitare ore e ore di messa a punto della prima, a
  niente messa a punto della seconda.}
  \label{img:accordaturagoethe}
}

\subsection{accordatura}

Il desiderio di camminare in equilibrio tra lo sviluppo della macchina e la discretizzazione della scrittura ha sollevato o attivato due questioni: la
formalizzazione di un sistema con regole e possibilità; la replicabilità del
risultato all'interno di quelle regole e possibilità. Come molte piccole cose
in questo racconto, anche sistema e replicabilità sono due componenti in
feedback tra loro: la ricerca della capacità di riprodurre relazioni
causa-effetto ha fatto sistema, quel sistema che definendosi ha reso possibile
una più efficace replicabilità dei risultati.

Uno dei nodi nevralgici del sistema, e quindi della relativa organizzazione in
suoni, è il processo di accordatura dello strumento. Definito, modificato,
ripensato, tale processo si è perfezionato in relazione con l'evoluzione
dell'ambiente di scrittura, del sistema di catalogazione dei materiali e con la conoscenza dello strumento maturata nelle esperienze concertistiche
concertistiche\footnote{%
  Nel 2022 all'interno del LEAP - Laboratorio ElettroAcustico Permanente di Roma,
  nasce il progetto LAZZARO: un laboratorio di interpretazione e prassi per lo
  studio del repertorio elettroacustico legato ai centri di ricerca e
  sperimentazione (Roma, Friburgo). All'interno di questo progetto lo strumento
  \tempo~è stato studiato approfonditamente e condotto in concerto con lo scopo
  di mantenere integro il rapporto con la sala da concerto, presente fin dal
  principio del progetto. Alcune date: 03 settembre 23 Goethe Institut Rom;
  11 ottobre 23 Conservatorio di Latina; 22 marzo 24 Goethe Institut Rom;
  13 luglio 24 Goethe Institut Rom.
}. La procedura che presento non è altro che lo specchio della consapevolezza
tecnica raggiunta nel controllo del mezzo. Il momento esatto in cui l'attuale
metodologia è stata definita cade in un luogo non ben definito del tempo tra
la scrittura del terzo e del quarto studio. La procedura che presento non è
l'unica possibile, è la più efficace tra altre scartate, ed è quella che
utilizzo per l'intero ciclo de \canto.

\subsection{prima posizione}

Lo strumento viene calibrato a partire dalla prima posizione $\alpha\beta$,
con pedale di tensione completamente carico ($p11$) e $fine-tuning$ a $0°$
e accordato sul $RE3_{442Hz}$. È altrsì eviente che scaricando la tensione
della membrana con il pedale si scende da quel $RE$ con continuità in un
glissato continuo per circa un'ottava (\ref{tikz:Iposambito}).

\begin{figure}[ht]
  \centering
  \includegraphics[width=.81\linewidth]{tikz/ambiti/I-ambito.pdf}
  \captionsetup{width=.81\linewidth}
  \caption{prima posizione da descrivere.}
  \label{tikz:Iposambito}
\end{figure}

\emph{Perché il $RE3$?}. Il $RE$ è stato trovato, all'interno dello strumento,
nell'inverno 2022. In quel periodo non c'era un interesse verso l'altezza
definita e stavo lavorando ad un processo di accordatura “fisica” ovvero di
accoppiamento risonante membrana-caldaia. Il processo di ricerca era il
seguente: accordato il timpano ai minimi della tensione, quindi poco sotto il suo ambito di riferimento (mettere riferimento), con i magneti posizionati
in prima posizione e l'induttore $Abbott$ (all'epoca si faceva chiamare $x$)
su $\alpha$, il sistema acustico risonante membrana-caldaia veniva ispezionato
con decine di ambiti sinusoidali indotti. L'obbiettivo era di selezionare dei set
di frequenze di accoppiamento tra i due elementi fisici. La tensione della
pelle è stata aumentata sia progressivamente mediante fine-tuning sia dai
registri di tensione e incrociata con gli ambiti sinusoidali. Si è arrivati poco
prima del $RE$ in una zona in cui il sistema annichiliva completamente
(circa $DO\#$ calante) per poi tornare con un'esplosione di vibrazioni in
risonanza attorno al $RE$.

Trovato l'accoppiamento, capito ed ascoltato lo strumento in quelle condizioni
privilegiate, si è cercato lo stesso tipo di suono ricco e potente in condizioni
di feedback, ovvero eliminando l'induzione dell'oscillatore sinusoidale e
attivando la catena elettroacustica completa. Il feedback si discosta
leggermente dall'altezza somministrata artificialmente ma è estremamente
reattivo per cui è piuttosto semplice portarlo ad altezza definita mediante i
gli appositi registri.

Il processo di apprendimento per ottenere una buona accordatura è durato più di tre
anni e può esere considerato il partner problematico della relazione scrittura
suono. Ho considerato concluso il viaggio eplorativo delle possibilità che mi ero
offerto con la catalogazione e la scrittura del IV studio. (DATA)

Ciò significa che i primi tre studi sono ricavati da cataloghi generati con
accordature “da viaggio”. Il IV studio è il primo che viene analizzato per
altezze prodotto proprio in virtù dell'esperienza esplorativa conclusa.

L'altezza non è mai stato un obiettivo.

Quando ho compreso, al terzo catalogo, che stavo per raggiungere la completa
solidità tecnica mi sono chiesto se avrei dovuto riscrivere completamente
anche i primi tre studi dopo aver testato la nuova tecnica nel IV. Decisi che
“no” i primi tre studi avrebbero testimoniato la loro funzione di studio, funzione
tecnologica di scrittura che evolve in feedback con lo strumento, in modalità
esplorativa, “da viaggio”. Non era un obiettivo, lo registro come una coincidenza:
i tre studi tecnici (quelli in cui la scrittura si concentra sulla risoluzione di
mettere a sistema la tecnica per l'interprete e che contemporaneamente appare
al musicista come studio di sola tecnica strumentale) sono il tempo di lavoro
in cui lo strumento si è compiuto tecnicamente.

% MISURARE L'ESTENSIONE AL COLPO
%
% Accordato lo strumento al $RE$ per la I posizione è pronto per l'intero ciclo
% del \canto, i duoi \studi[\ldots]
%
% DESCRIZIONE DELLA TECNICA DI FEEDBACK PER L'INTONAZIONE



Accordato il $RE$ sulla prima posizione si è adottato un criterio di estensione
delle altezze di riferimento con le posizioni successive. La I posizione $\alpha\beta$
condivide $\alpha$ con la IV posizione $\alpha\gamma$ e la VI posizione $\alpha\delta$.
\marginpar{grafico di collegamento tra le posizioni.}
Il solo cambio di posizione del secondo magnete produce una variazione dell'altezza
del feedback (sul magnete in movimentoa causato dalla variazione della distanza
del magnete dai pilastri microfonici) assecondata per ottenere altezze temperate d'effetto
di riferimento.

\subsection{seconda posizione}
Ottimizzare l'accoppiamento trovato attorno al $RE$
in prima posizione è stato un percorso altrettanto lungo.

È stato registrato
il catalogo e scritto il primo sudio su quella posizione con quella accordatura
e solo molto tempo dopo, per il catalogo della seconda posizione ($\beta\gamma$)
e per il secondo studio sono iniziate le questioni su come muovere l'accordatura
tra le posizioni, sempre nella ricerca del migliore accoppiamento
membrana-caldaia a massima tensione del pedale.


\subsection{terza posizione}

\subsection{quarta posizione}

PERCHé?

perchè avere altezze definite?

costruzione del suono altezza perte del timbro definizione di timbro e scrittura.

La costruzione di uno strumento necessità di conoscenze tecniche ma a guidare,
più degli oggetti, sono le idee e i perché da risolvere.

Uno strumento libero dal sistema temperato, libero da strutture spettrali rigide
e vincolanti. Uno strumento a membrana risponde a entrambi i problemi. Una volta
elaborata la strategia di costruzione timbrica descrivere i comportamenti sonori
contempla descrivere ogni sua parte: durata, intensità e altezza.

\subsection{quinta posizione}

\subsection{sesta posizione}




\begin{table*}
\centering
{\renewcommand{\arraystretch}{0.5}%
\begin{tabularx}{1\linewidth}{X}
\adjustbox{valign=c}{%
  \includegraphics[
  %trim={10mm 0mm 0mm 0mm},clip,
  width=.95\textwidth,
  height=0.95\textheight,
  keepaspectratio
  %scale=1,
  %[trim={left bottom right top},clip]
  ]{tikz/ambiti/I-ambito.pdf}}\\
  %
\adjustbox{valign=c}{%
  \includegraphics[
  %trim={10mm 0mm 0mm 0mm},clip,
  width=.95\textwidth,
  height=0.95\textheight,
  keepaspectratio
  %scale=1,
  %[trim={left bottom right top},clip]
  ]{tikz/ambiti/II-ambito.pdf}}\\
  %
\adjustbox{valign=c}{%
  \includegraphics[
  %trim={10mm 0mm 0mm 0mm},clip,
  width=.95\textwidth,
  height=0.95\textheight,
  keepaspectratio
  %scale=1,
  %[trim={left bottom right top},clip]
  ]{tikz/ambiti/III-ambito.pdf}}\\
  %
\adjustbox{valign=c}{%
  \includegraphics[
  %trim={10mm 0mm 0mm 0mm},clip,
  width=.95\textwidth,
  height=0.95\textheight,
  keepaspectratio
  %scale=1,
  %[trim={left bottom right top},clip]
  ]{tikz/ambiti/IV-ambito.pdf}}\\
  %
\adjustbox{valign=c}{%
  \includegraphics[
  %trim={10mm 0mm 0mm 0mm},clip,
  width=.95\textwidth,
  height=0.95\textheight,
  keepaspectratio
  %scale=1,
  %[trim={left bottom right top},clip]
  ]{tikz/ambiti/V-ambito.pdf}}\\
  %
\adjustbox{valign=c}{%
  \includegraphics[
  %trim={10mm 0mm 0mm 0mm},clip,
  width=.95\textwidth,
  height=0.95\textheight,
  keepaspectratio
  %scale=1,
  %[trim={left bottom right top},clip]
  ]{tikz/ambiti/VI-ambito.pdf}}
\end{tabularx}}
\caption{Tavola sinottica degli ambiti relativi alle sei posizioni. }
\label{tab:ambiti}
\end{table*}

\clearpage
