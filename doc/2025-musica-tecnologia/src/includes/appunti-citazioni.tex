%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../099-articolo-musica-tecnologia.tex
%

Nel descrivere l'attitudine di ricerca musicale sullo sfondo del \emph{canto},
mi servo di un riferimento letterario a me caro: Luigi Nono.

\begin{quote}
La musica non e` solo composizione. \\
Non è artigianato, non è un mestiere. \\
La musica è pensiero. \cite{Nono1985}
\end{quote}

Il tema dell'ascolto in Nono è meravigliosamente descritto da Cacciari
\cite{Cacciari1995} che nel suo ragionamento disegna lo sfondo di una
problematica che assale l'intera umanità, che rende necessario comprendere
problemi di carattere filosofico nelle relazioni tra scrittura-voce-ascolto.
Problemi di ordine generale che si riferiscono al significato culturale
generale (europeo, occidentale) della nascita della scrittura alfabetica. È
questo un lento processo che richiede migliaia di anni e vede il dominio della
visione sull'ascolto dalla diffusione del testo omerico, scritto, attorno al VII
sec a.C.

Con la scrittura avviene che le cellule fonetiche elementari si rendono visibili,
la sequenza della voce viene rappresentata visivamente,

\begin{quote}
  garantita, non è più aleatoria, affidata all'aleatorietà della voce. Non è una
  rappresentazione empirica, ma una classificazione ideale\cite{Cacciari1995}.
\end{quote}

Prima di questa rivoluzione per comprendere occorreva ascoltare e parlare.
Cacciari la definisce una

\begin{quote}
  progressiva desomatizzazione della voce. Una perdita dell'udire. L'udire
non è più una funzione fondamentale del comprendersi\cite{Cacciari1995}.
\end{quote}

Questo è il processo che progressivamente conduce al primato della scrittura
e della visione. Sottolinea inffatti come la radice greca del verbo vedere sia
la stessa del verbo sapere, \emph{dunque vedere è sapere}.

È evidente che la scrittura non può contenere tutti i \emph{gesti di una voce.}
\emph{Logos}, dire, pronunciare. A questo fa riferimento un fatto importante,
prima di questa \emph{colossale rivoluzione} i testi filosofici avevano una metrica,
non erano in prosa, per ricordare attraverso i metri, i ritmi. Il canto, il
cantore, articolavano la memoria. Con la scrittura, la funzione del metro e del
ritmo, della poesia e del canto, viene meno. E questo venire meno consiste anche
nella morte della memoria perchè ricorda, per noi, la scrittura. Il dominio della
visione, della scrittura, la perdita della memoria e del rapporto con la voce e
con un parlante.

Cacciari sottolinea che in molte pratiche artistiche questo problema può essere
dimenticato, può non essere un problema, ma non nella musica: la musica non può
essere senza ascolto. La perdita della memoria, dell'ascolto, della memoria
dell'ascolto è fatale per la musica. Questo accade perchè ad ogni ascolto muta
il testo stesso. La musica non può esistere senza un ascolto vivente, attivo.

Questo è il territorio in cui si svolge il \canto. La musica che costringe
all'ascolto, al silenzio, che non ammette distrazioni, che vuole il primo
ascoltatore proprio nell'interprete, il cantore. La tensione dialettica tra
silenzio, parola e canto, dove l'ascoltare passa per un ascoltarsi, il ricordare
passa prima per un ricordarsi. L'interprete che per primo, cercando di
comprendere, di capire che vorrebbe riuscire a comprendere di più ciò che c'è
\emph{prima del primo suono e dopo l'ultimo suono}\cite{Cacciari1995}, si
fa testimone della ricerca musicale nel luogo sonoro. Non inventare solo
l'ascoltatore con una musica \emph{non da} ascoltare ma \emph{per} ascoltare
(Branchi) ma inventare, prima (anche) il cantore, l'interprete in grado di
\emph{articolare, accentuare, dare al canto}, il testo musicale.
\emph{L'interprete non legge} il testo musicale, \emph{lo riattiva, lo da al
canto}\cite{Cacciari1995}. E la dialettica tra parola, visione, silenzio
sonoro, poesia e canto costruisce una dimensione corale, sociale e politica
dell'ascolto.

\begin{quote}
La funzione di ogni lavoro consiste nell'aprire sensi,
aprire interrogazioni, far risuonare interrogazioni, far risuonare
interpretazioni diverse, polemiche, dar senso, fornire sensi, dar vita a
interpreti e interpretazioni. Accentuare, provocare al canto. Ascoltare, dar
senso, è una vera rivoluzione culturale\cite{Cacciari1995}.
\end{quote}

% \begin{quote}
%   Cioè occorre concepire l'uomo come una serie di rapporti attivi (un processo)
%   in cui se l'individualità ha la massima importanza, non è però il solo
%   elemento da considerare. L'umanità che si riflette in ogni individualità è
%   composta da diversi elementi: 1) l'individualismo 2) gli altri uomini 3) la
%   natura. [\ldots] l'individuo non entra in rapporti con gli altri uomini per
%   giustapposizione, ma organicamente, cioè in quanto entra a far parte di
%   organismi dai più semplici ai più complessi. Così l'uomo non entra in rapporti
%   con la natura semplicemente per il fatto di essere egli stesso natura, ma
%   attivamente, per mezzo del lavoro e della tecnica.
% \end{quote}% GRAMSCI P28

% \begin{quote}
%   Il progresso è una ideologia, il divenire è una concezione filosofica.
% \end{quote}% GRAMSCI P32

% \begin{quote}
%   Secoli e secoli di idealismo non hanno mancato di influire sulla realtà.
% \end{quote}% BORGES
%
% \begin{quote}
%   [208] Tutta la rappresentazione è falsa. Una somiglianza è necessariamente
%   diversa da ciò che rappresenta. Se così non fosse, sarebbe ciò che
%   rappresenta, e quindi non una rappresentazione. La sola rappresentazione
%   autenticamente falsa è il credere nella possibilità di una rappresentazione
%   autentica.
% \end{quote}% WARK P22

% \begin{quote}
%   Posto il principio che tutti gli uomini sono «filosofi», che cioè tra i
%   filosofi professionali o «tecnici» e gli altri uomini non c'è differenza
%   «qualitativa» ma solo «quantitativa» (e in questo caso quantità ha un
%   significato suo particolare, che non può essere confuso con somma aritmetica,
%   poiché indica magiore o minore «omogeneità», «coerenza», «logicità» ecc,
%   cioè quantità di elementi qualitativi), è tuttavia da vedere in che consista
%   propriamente la differenza.
% \end{quote}% GRAMSCI P24

% \begin{quote}
%   perciò si può dire che la personalità storica di un filosofo individuale è data
%   anche dal rapporto attivo tra lui e l'ambiente culturale che egli vuole
%   modificare, ambiente che reagisce sul filosofo e costringendolo a una continua
%   autocritica, funziona da «maestro».
% \end{quote}% GRAMSCI P24
