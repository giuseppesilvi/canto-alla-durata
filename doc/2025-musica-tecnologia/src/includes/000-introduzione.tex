%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../099-articolo-musica-tecnologia.tex
%
\addcontentsline{toc}{section}{INTRODUZIONE}
\section*{INTRODUZIONE}
%
Oltre la presente introduzione, nella quale cerco di edificare l'\gls{ambiente}
linguistico e storico in cui la mia attività musicale prende forma, e la coda,
dove in un \gls{glossarietto} raccolgo parole che necessitano un tempo
aggiuntivo, il testo che segue è organizzato in tre parti, ognuna delle quali
partecipa alla modulazione del feedback su più livelli di pensiero: percorsi
disposti in serie che, tuttavia, raccontano processi paralleli in relazione
organica, a costruzione di un'interfaccia musicale nella relazione
uomo/ambiente\footnote{%
Con \emph{ambiente} per indicare la natura fisica dello spazio con cui entriamo
in relazione. Di volta in volta, l'ambiente può essere un \emph{timpano}, l'altro
timpano, una sala da concerto, una poesia.
}:
%
\begin{description}
  \item[feedback: creatività, ciclo base:] dove introduco gli aspetti teorici del
    lavoro di ricerca musicale. Mi concentro sulla definizione di alcune parole,
    cardini del racconto, al fine di catalogare \cite{ferraris2014}
    gli oggetti che compongono il laboratorio intellettivo, lo scenario, in una
    sorta di \emph{presentazioni} e \emph{puntualizzazioni} per lettori lenti,
    senza fretta.
  \item[feedback: ascoltatore privilegiato:] in cui il racconto volge sugli
    aspetti poetici e poietici che attualmente assumono la forma del progetto
    \canto\footnote{2021$\sim$oggi}. L'indagine scende ad un livello formale più
    basso, più intimo, nel tentativo di far emergere i \emph{perché} di alcune
    scelte, la storia e la struttura dell'opera, quindi, in potenza, è la
    sezione meno interessante, per trichechi della lettura.
  \item[feedback: materia come strumento] in cui descrivo tecnologicamente
    l'impiego del feedback come processo fondante dello strumento
    \tempo\footnote{%
    \emph{Timpani Electro Magnetic Pulse Oscillation:}, timpano aumentato a
    induzione elettromagnetica.
    } e le implicazioni che questo ha riversato nella scrittura musicale.
    L'interesse per il feedback come oggetto esclusivamente tecnologico potrebbe
    portare una tipologia di lettore a saltare tutto il resto, per giungere qui.
    %Per questo motivo in questa sezione sono ridotte al minimo le digressioni di
    %carattere teorico.
\end{description}

Per introdurre un minimo di ambientazione storica e sociale, in cui la presente
trattazione abita, inizio il racconto partendo da dubbi antichi che si
tramandano per generazioni e che, con qualche espediente narrativo, proverò a
far convergere verso l'interno della spirale:

\begin{quote}
  [\ldots] E quando ancora Dallapiccola mi informa che la
  \emph{Sonata per due pianoforti} di Strawinsky e il \emph{Quartetto} in mi
  minore di Hindemith costituiscono delle riuscite, ma rappresentano una
  posizione di «staticità» e di rinuncia «a tutto ciò che può essere o sembrare
  ricerca», ancora io mi allarmo, perché mi vedo improvvisamente tolto di mano
  il solito criterio valido di giudizio estetico, che è il discernimento del
  bello e del brutto, e sostituito con certi concetti ibridi e spuri – la
  ricerca, il nuovo – che non so di dove diavolo vengano fuori e quale
  giustificazione abbiano. \cite{mm:eme56}
\end{quote}% MILA 32

\noindent (la ricerca e il nuovo) \emph{Di dove diavolo vengono fuori?}\\
\emph{E quale giustificazione hanno?}

La prima domanda stuzzica la necessità di una storia della musica organizzata
attorno all'elaborazione di una storia della ricerca musicale. Per questo motivo
provo ad introdurre il lavoro sul \canto~descrivendo la terra fertile su cui ho
impostato la ricerca personale, descrivendone le origini sociali:
\emph{di dove diavolo viene fuori}. Un quadro storico e sociale, la descrizione
di una comunità in grado di condividere musica, e quindi pensiero, può forse
evitarmi di dover rispondere per esteso alla seconda domanda.

Il \emph{discernimento del bello} ha condotto alla forma dominante di
presentazione della storia della musica occidentale come sequenza discreta di
opere d'arte, emersioni uniche e distinte, con caratteristiche diverse. Tuttavia,
tali emersioni discrete, dalle tendenze tanto più impossibili quanto più artistiche,
si rendono possibili sul tempo continuo delle possibilità tecniche e tecnologiche.
Dove un'opera d'arte può essere completamente diversa dalle adiacenti (questo è
anche un tratto caratteristico del rumore), la storia di quella tecnologia, che
genericamente chiamiamo storia degli strumenti musicali, si caratterizza per un
continuo lavoro di elaborazione \gls{tecnica} espresso nel rapporto uomo/macchina.
Così un clarinetto rinnovato rende possibile la sistematizzazione del linguaggio
per Mozart; uno strumento dimenticato rende impossibile una nuova musica, diversa;
un pianoforte vizia il pensiero musicale irrimediabilmente (che nella vita di un
singolo essere umano è un \emph{per sempre}).

\begin{quote}
  [\ldots] la filosofia di un'epoca non è la filosofia di uno o un altro
  filosofo, di uno o un altro gruppo di intellettuali, di una o altra grande
  partizione delle  masse popolari: è una combinazione di tutti questi elementi
  che culmina in una determinata direzione, in cui il suo culminare diventa
  norma d'azione collettiva, cioè diventa «storia» concreta e completa
  (integrale). \cite{ag:matst}
\end{quote}% GRAMSCI P22

Per comprendere a pieno l'evoluzione degli strumenti musicali è necessario
vederla girovagare nel tempo, afferrare il momento in cui, lungo il tortuoso
percorso, gli si affianca lo sviluppo creativo della scrittura musicale
definendo le due tecnologie intrecciate su cui si articola la possibilità di
creare musica: due forme di trascrizione aleatoria tecnologicamente avanzata di
diverse continuità. Due tecnologie che condizionano e si fanno condizionare dal
linguaggio, dopo tutto anche con le parole condizioniamo questo percorso
evolutivo: padroneggiando qualità sonore, dall'avvento della parola
\emph{timbro}\footnote{%
  la scoperta del \emph{timbro} (o l'introduzione della parola in musica)
  avvenuta nei primi anni del novecento, ha posto le basi filosofiche per
  ristabilire equilibri di relazione tra suono ed espressione musicale di
  correlazione uomo/ambiente.
}
in musica abbiamo iniziato a catalogare \emph{suoni ordinari},
diversi dai suoni prodotti mediante \emph{tecniche estese}, per approdare
all'idea degli \emph{strumenti aumentati}\footnote{%
  La letteratura attuale si pone in maniera confusa sul concetto di strumento
  aumentato. In questo ambito di ricerca si possono considerare
  \emph{strumenti aumentati} solo quelli che mantengono integro il corpus delle
  potenzialità fisiche e musicali pre-esistenti: che si pongano dunque come
  sistemi nel processo di sviluppo degli strumenti, e quindi del pensiero, in
  continuità con il passato, senza impedirlo.
}.
Nondimeno, la cristallizzazione tecnica a cui sono soggetti alcuni strumenti
musicali, non evoluti da secoli, appartiene (ed è prerogativa) all'industria
dell'intrattenimento, non alla musica, non alla ricerca che la anima senza soluzione
di continuità, da che l'uomo ha memoria.

La storia della tecnica sostiene la cronologia delle opere d'arte e garantisce
l'intero percorso di evoluzione e ricerca attorno a problematiche di produzione
controllata di vibrazioni acustiche. Un catalogo generoso di strumenti musicali,
quindi il catalogo della tecnica disponibile alla creatività musicale, quindi un
catalogo degli strumenti di pensiero musicale, dovrebbe contenere, insieme alla
voce umana, alle pelli, al legno percosso e a quello messo in vibrazione con
l'aria (e via dicendo), dovrebbe contemplare anche la scrittura e il feedback.

\begin{quote}
  Nella discussione scientifica, poiché si suppone che l'interesse sia la
  ricerca della verità e il progresso della scienza, si dimostra più «avanzato»
  chi si pone dal punto di vista che l'avversario può esprimere un'esigenza che
  deve essere incorporata, sia pure come un momento subordinato, nella propria
  costruzione. Comprendere e valutare realisticamente la posizione e le ragioni
  dell'avversario (e talvolta è avversario tutto il pensiero del passato)
  significa appunto essersi liberato della prigione delle ideologie (nel senso
  deteriore, di cieco fanatismo ideologico), cioè porsi da un punto di vista
  «critico», l'unico fecondo nella ricerca scientifica. \cite{ag:matst}
\end{quote}% GRAMSCI P21

Il feedback, liberato dalla casualità della sua apparizione, della sua
\emph{apparibilità}, diviene anch'esso strumento musicale e leva per la
costruzione di strumenti musicali. La scrittura, come leva di ricerca, attiva
possibilità musicali inaudite. Ciò la rende diversa, opposta e indisposta,
alla scrittura per la scrittura.

\begin{quote}
  [\ldots] C'erano soltanto pile di studi, di esercizi retorici, di copie del
  passato: pagine e pagine [\ldots] avendo cominciato tardi ad occuparmi
  seriamente di musica ed essendomi trovato a doverlo fare in un ambiente a dir
  poco conservatore, la terra mi scottava sotto i piedi\ldots~Non è bello dover
  studiare la grammatica alla soglia dei vent'anni, quando una maturità
  \emph{illegittima}, ma già indirettamente e fermamente conquistata, schiude con
  violenza ben altri orizzonti! E quando poi tale grammatica, allora, si riferiva
  ad una lingua non più in vigore. Era quasi come andare alla deriva di un
  vagone spinto lungo un binario morto\ldots~Cioè, mi correggo: non si trattava
  di deriva, occorreva vogare eccome! Non c'era tempo di fare altro. Nemmeno di
  pensare (forse questo era il vero scopo del timoniere: impedire il pensiero,
  la riflessione)\cite{mb:rmifddXeXII}.
\end{quote}% BERTONCINI P32

Bello, brutto, scrittura, analisi, grammatiche, sintassi e linguaggio: senza una
partecipazione attivistica della pratica musicale, tutti questi si trasformano in
spazi fuori dal tempo. Il tempo risiede nel processo di fare di sé qualcosa di
dignitosamente riconoscibile davanti ad uno specchio:

\begin{quote}
  [\ldots] pensai che sarebbe stato giusto e importante (non soltanto per noi!)
  dare l'avvio ad una attività fondata sul nostro \emph{volontariato}, si direbbe
  oggi, e creare così un'associazione musicale il cui nucleo portante fosse
  appunto costituito da compositori in grado, in alcuni casi, di esercitare al
  tempo stesso con assoluta dignità professionale, anche il ruolo di esecutore.
  \cite{mb:rmifddXeXII}.
\end{quote}% BERTONCINI P59
%
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
%
Ogni storia è unica, personale e intima. Anche se nel tempo si intreccia con
l'organicità della storia dell'uomo, negli occhi dei singoli protagonisti che
raccontano una storia c'è sempre quello spirito unico, non raccontabile, che
nel racconto si articola con le parole in altri sensi. Da Petrassi, a Guaccero,
a Nottoli, a Lupone, per indicare solo il ramo centrale che dal tronco arriva a
me, racconto una storia di Roma sapendo che molte delle cose che ho vissuto non
sono raccontabili. Roma è una città con una peculiare storia di strumentazione
elettronica ed elettroacustica di invenzione\footnote{%
  \label{def:invenzione}
  dal latino \emph{inventio} «ritrovamento»; in tedesco è \emph{Erfindung} e il
  dizionario propone come esempi di invenzioni rappresentative la ruota e il
  computer, entrambi in continuità con ciò che già si stava cercando:
  \emph{finden}, trovare: l'invenzione è un processo euristico.
} con la quale ho cercato di confrontarmi affinché fossi in grado di comporre,
nel tempo, un pensiero generale.

L'attività della composizione coincidente con una forte matrice di ricerca
musicale che attinge e spinge verso una ricerca scientifica e tecnologica è il
presupposto al pensiero elettronico che ha dato vita, nel novecento, a scuole di
musica elettronica in tutta europa. Tuttavia, a Roma la tecnologia non è mai
stata esterna, appendice, utensile dell'attività musicale come è accaduto in
altre città in cui la musica elettronica ha mosso i primi passi. La musica
elettronica romana nasce innanzi tutto come scuola di pensiero, forse perché in
un primo momento non c'era elettronica da manipolare.

A partire dagli anni cinquanta del novecento, in brevi archi di tempo, si può
assistere alla nascita di strumenti musicali unici di \emph{liuteria elettronica}
(\emph{Fonosynth}, \emph{Syn-Ket})\cite{pizzaleo2014liutaio}; di studi
privati condivisi\cite{guaccero2005iter}; di associati attorno ad
un'intensa attività concertistica esclusivamente dedicata alla musica
contemporanea (\emph{Nuova Consonanza})\cite{lz:evangelisti}; di una
società di informatica musicale (\emph{SIM})\cite{bianco} dalla quale
poi nasce il \emph{CRM - Centro Ricerce Musicali} che tutt'oggi opera
nell'alternanza di ricerca e proposta sociale con il festival
\emph{ArteScienza} con oltre trenta edizioni alle spalle. Tutto ciò è nato tra
le mani di musicisti che poi hanno dato vita alle prime scuole di musica
elettronica che successivamente si sono riversate nei conservatori italiani.
In uno di questi, a Roma, per dieci anni si è tenuto \emph{EMUFest}
(\emph{Festival Internazionale di Musica Elettronica} del Conservatorio S.
Cecilia di Roma, ideato da Giorgio Nottoli).

In tutto questo percorso l'attività romana non ha mai utilizzato la tecnologia:
l'ha ideata, l'ha plasmata, seguendo l'idea di un pensiero elettronico libero
dagli utensili, mezzi, creandone di nuovi a leve di indagine musicale. In quel
pensare c'è stato spazio per la creazione musicale, proveniente da
una \emph{scuola del fare} (Giorgio Nottoli), nel suo rapporto complesso tra
\emph{opera-strumento-interprete} (Michelangelo Lupone).
\marginpar{%
  \includegraphics[width=\marginparwidth]{tikz/o-s-i/o-s-i.pdf}
  \captionof{figure}{\raggedright Questo grafico descrive la relazione che
  intercorre tra \emph{opera, strumento e interprete} nell'opera compositiva di
  Michelangelo Lupone. Il grafico qui riprodotto viene dagli appunti presi durante
  il seminario sul \emph{WindBack} tenuto da Lupone il 6 ottobre 2016 per il
  Festival Aumentazioni del Conservatorio di Salerno ed è da intendersi come una
  rete di relazioni bidirezionali tutti verso tutti. Il progetto musicale
  quindi si dipana su queste relazioni con il desiderio di mantenere inalterate
  le proporzioni di relazione.} \label{img:osi}
}

\begin{quote}
  \ldots gli strumenti veicolano le idee musicali, e danno corpo materiale e
  finito all’espressione delle emozioni. Nel caso di \emph{Gran Cassa}, l’opera
  e lo strumento sono frutto di una stessa intuizione e necessità creative. [\ldots]
  ho messo in relazione la teoria matematica delle vibrazioni con la risposta
  acustica dello strumento, fino ad intuire la strada [\ldots]
  disegnavo e apportavo modifiche strutturali allo strumento. L’opera e lo
  strumento sono nati così, da un’interazione inscindibile fra suono, segno
  grafico, tecnica esecutiva, musica e mezzo espressivo\footnote{%
  \emph{Music@}, rivista bimestrale del Conservatorio \emph{Alfredo Casella},
  L'Aquila. Anno III, N. 9, LUG-AGO 2008. \emph{Tecnologie: Gran Cassa per
  Napoleone Intervista a Michelangelo Lupone} di Carlo Laurenzi.}.
\end{quote}

Il corpo di questa dissertazione, i progetti in essa raccontati, sono esiti
provvisori dell'attività di ricerca iniziata durante il corso libero di
\emph{Musica Elettronica - Indirizzo Specialistico - Strumenti aumentati}
presso l'\emph{Accademia Nazionale Santa Cecilia} di Roma tenuto da Michelangelo
Lupone.
%
Il lavoro in Accademia inizia il ventitré dicembre duemila ventuno\footnote{%
I diari del corso e tutti i materiali di ricerca sono archiviati su:
\url{https://gitlab.com/giuseppesilvi/canto-alla-durata}}. Negli appunti
presi durante la prima lezione c’è la descrizione di un progetto senza titolo
per quattro strumenti aumentati. Seguono pagine di appunti e progettazione. La
data successiva è del cinque gennaio duemila ventidue. È già aperto il fascicolo
\canto, il progetto è già in movimento e il concerto finale del corso è
avvenuto il ventuno giugno duemila ventidue\footnote{%
  Gli esiti del concerto hanno stimolato una riflessione personale sulle funzioni
  etiche ed estetiche di una ricerca musicale, accessibile su:
  \url{https://gitlab.com/giuseppesilvi/perche-siete-qui}
}. In quei sei mesi di progettazione vengono ideati tre dei quattro strumenti
aumentati componenti il corpo strumentale; il quarto fu il sistema
\emph{WindBack} di Lupone\cite{windback}. L'obiettivo primario fu quello
di arrivare al concerto conclusivo, con tutti i prototipi sviluppati ed un
apparato teorico sulla messa in scena elettroacustica, in modo da poter testare
l'intero progetto, anche se con ogni singolo anello della catena al livello zero,
in un'idea di concerto-laboratorio necessario e integrato alla ricerca musicale.

I primi appunti su una tecnica di scrittura consapevole, concepita espressamente
per il timpano aumentato arrivano il due agosto duemila ventidue. Fu il primo
vero risultato indotto dal concerto, quasi due mesi dopo, la prima vera prova
tangibile, a conferma della necessità di introdurre la sala da concerto (e
quindi l'esecuzione pubblica) negli strumenti di pensiero che collaborano alla
messa a punto di una pratica creativa: ha condotto ad una prima idea, concreta
e risolutiva, di scrittura. % direttamente con i logogrammi.

\begin{figure}[t]
  \begin{addmargin*}[0pt]{-\marginparsep-\marginparwidth}
    \centering
    \includegraphics[width=.98\linewidth]{images/IMG_8166.jpeg}
    \captionsetup{width=.81\linewidth}
    \caption{Concerto, \emph{Teatro Studio, Auditorium Parco della Musica},
    21 giugno 2022. Interpreti: Marina Boselli - Euphonium \emph{Bo.Si.},
    Virginia Guidi - Voce \emph{diminuita}, Enzo Filippetti - Sassofono
    \emph{WindBack}, Marco di Gasbarro - \tempo, Giuseppe Silvi - Regia del
    Suono. Ai quattro interpreti sono alternati altrettanti diffusori sferici
    \emph{S.T.ONE}\cite{gs:pto} e su di essi, al centro dell'ottagono, è
    sospeso un diffusore sferico \emph{S.T.ON3L}\cite{gs:pto}, in un idea di
    messa in suono tridimensionale.}
  \end{addmargin*}
  \label{img:concerto}
\end{figure}

Una peculiarità della ricerca che svolgo riguarda quindi i luoghi: laboratorio
e sala da concerto. Il laboratorio musicale, la bottega, è il luogo in cui
l'attività quotidiana produce informazione. Quest'informazione è accessibile a
chiunque frequenti attivamente il laboratorio, un processo in divenire. Nel
luogo del laboratorio, si accede al luogo degli strumenti musicali con metodo
scientifico, ma il percorso si biforca spesso in un parallelismo di secondo
ordine, dove gli strumenti scientifici si aumentano del metodo musicale. La
sala da concerto rientra pienamente nel circuito di ricerca, non è una vetrina
ma l'ultimo nodo di somma dei processi messi in atto: all'uscita dalla sala da
concerto, ci si dirige nel tempo nuovo al punto di inizio, il laboratorio, e
nulla potrà essere come l'attimo prima della somma.

Nel \canto~ho cercato di affrontare diverse idee di feedback: dalle ricerche
sul feedback a induzione elettromagnetica che hanno dato vita allo strumento
\tempo, al sistema \emph{Bo.Si.} (sordina elettroacustica klein per euphonium),
alla voce diminuita mediante automodulazione. Su questi principi tecnici di
impiego del feedback elettroacustico sono stati svolti esercizi con gli
interpreti\footnote{
Alcuni degli esercizi svolti sono visibili su \url{https://vimeo.com/user111479998}.
}
che hanno alimentato altri livelli di pensiero attorno al feedback che cercherò
di sviluppare nel corso della trattazione.

\clearpage
