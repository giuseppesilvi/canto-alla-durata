%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../099-articolo-musica-tecnologia.tex
%
%-------------------------------------------------------------------------------
%--------------------------------------------------------------------- SECTION -
%-------------------------------------------------------------------------------
\section{feedback: creatività, ciclo base}

Percorrere la lunga storia della fantasia creativa occidentale, da Aristotele a
Fagioli \cite{mf:istinto}, con un paio di soste lunghe nei pressi di
Bergson \cite{hb:materia} e Sartre \cite{jps:immaginario}, attraverso le questioni
musicali lucidamente illustrate da Cacciari \cite{Cacciari1995}, ha prodotto un
risultato inatteso, un'assenza: non ho \emph{alternative sonore} alle parole
immaginazione e immaginario; non ho un lessico indipendente dalla visione,
autonomo dal visibile per ciò che conctraddistingue la \emph{fantasia sonora};
l'immaginazione, l'immaginario, spesso si sostituiscono generalizzando fantasia
e creatività, amplificando l'assenza di una parola indipendente e necessaria per
l'attività creatrice sonora.

Penso suoni, ricordo suoni, (\emph{sogno suoni?}) e più mi impegno nel farlo e
più trovo che l'attività che faccio abbia poco a che vedere con l'immaginazione;
l'attività creativa di \emph{sonazione}, di invenzione timbrica, di messa a
punto di una tecnica d'indagine estesa dentro gli strumenti musicali
\cite{netti23} conduce alla costruzione di un \emph{sonario} rappresentabile sia
nel processo di scrittura (in funzione di riduzione simbolica) sia nella
relazione di dialogo possibile ed esclusivo tra musicisti.
%-------------------------------------------------------------------------------
%----------------------------------------------------------------- SUB-SECTION -
%-------------------------------------------------------------------------------
\subsection{creatività e allucinazione}

Una definizione di \gls{allucinazione} indica un «\emph{\gls{fenomeno}} psichico,
provocato da cause diverse, per cui un individuo percepisce come reale ciò che
è solo \emph{immaginario}.» Immaginario, «che è \emph{effetto} d'immaginazione»
in contrapposizione con reale, «che è, che \emph{esiste effettivamente} e
concretamente, non illusorio, immaginario o possibile». Quindi il reale non è
possibile, in quanto reale, mentre l'immaginario può essere possibile.

\begin{quote}
  La possibilità non è la realtà, ma è anch'essa una realtà: che l'uomo possa
  fare una cosa o non possa farla, ha la sua importanza per valutare ciò che
  realmente si fa. Possibilità vuol dire libertà. \cite{ag:matst}
\end{quote}% GRAMSCI P35

L'immaginario può essere possibile; possibilità vuol dire libertà: la creatività,
libera, è un percorso di trasduzione da un'informazione immaginaria a
un'informazione possibile: un processo possibile dall'idea alla cosa.

Ma l'immaginario, quando collettivo, è anche una cosa, un \emph{insieme di
simboli e concetti presenti nella memoria} di una comunità: una memoria
collettiva. L'\emph{insieme di rappresentazioni simboliche}.

Quindi la creatività libera e contemporanea \cite{agamben2008che} del singolo
(quella possibile, immaginaria, inattuale) si stacca dall'immaginario collettivo
che è fondamentalmente memoria, passato, passato di altri, in una forma di
rappresentazione privata, in attesa di condivisione, in attesa forse di divenire
memoria collettiva di altri, di un altro tempo.

\begin{quote}
  Il punto cioè in cui la concezione del mondo, la contemplazione, la filosofia
  diventano «reali» perché tendono a modificare il mondo, a rovesciare la prassi.
  \cite{ag:matst}
\end{quote}% GRAMSCI P41

L'allucinazione è anche descritta come \emph{turbamento mentale}, come la
comparsa di \emph{immagini sensoriali} dotate di \emph{piena evidenza realistica},
che vengono posizionate, inquadrate, viste, udite, sentite, come sensazioni,
nella realtà esterna, formate, tuttavia, per un processo interno, anche in
assenza o senza che sia presente, o esista, l'oggetto o il fatto corrispondente.

\begin{quote}
  Creativo, occorre intenderlo quindi nel senso «relativo», di pensiero che
  modifica il modo di sentire del maggior numero e quindi la realtà stessa che
  non può essere pensata senza questo maggior numero. Creativo anche nel senso
  che insegna come non esista una «realtà» per se stante, in sé per sé, ma in
  rapporto storico con gli uomini che la modificano, ecc\ldots \cite{ag:matst}
\end{quote}% GRAMSCI P23

Per portare questo quadro, questa diagnosi, all'interno del processo creativo
sonoro, accedere alla rappresentazione, quindi alla \emph{sonazione} nella
costruzione di un \emph{sonario} personale e condivisibile è necessario
soffermarsi sul sensibile, sugli oggetti che dall'ambiente diventano coscienza,
a volte conoscienza.

Come suggerisce Domenico Guaccero \cite{branchi1977tecnologia} riparto
dall'osservazione dei fenomeni cercando la relazione organica con
l'ambiente, nel tentativo di condividere un primo prototipo di \emph{sonario}.
Il fenomeno sotto osservazione è la vibrazione acustica, quella particolare
vibrazione che rientra nell'ambito multidimensionale\footnote{%
  Un fenomeno acustico è udibile se soddisfa tutte e tre le dimensioni: tempo,
  ampiezza, frequenza.
} dell'udibile: la vibrazione è udibile, energia potenziale. Il fenomeno fisico
è esterno, e l'organo dell'udito è l'entrata, verso l'interno dell'uditore.
Fuori da quell'ambito c'è l'inudibile, dentro quell'ambito c'è l'udibile. Il
grado successivo di intimazione è il sentire: non si è più fuori, si è già
dentro l'organo di senso.

\begin{quote}
  Il senso in musica è l'interfaccia uomo/musica. \cite{stefani}
\end{quote}

La vibrazione sensibile, dentro e dopo le soglie di udibilità, entra in
relazione con la sensibilità individuale. È quello che nel senso comune
chimiamo suono, la rappresentazione della vibrazione acustica.

Il suono non è la vibrazione stessa ma il residuo di quella sensazione specifica
di quella vibrazione acustica. Non è fuori, ma dentro. È esperienza, quindi
memoria, quindi coscienza. Mentre la vibrazione è attivazione del meccanismo
uditivo, il suono è un'esperienza della mente. Questo apre ad una soglia
invalicabile: il silenzio è l'esperienza inaccessibile. La non-esperienza che
attrae e che apre ad altre esperienze (sonore). Luigi Nono, John
Cage\ldots~nuovi suoni in cerca di silenzio, verso nuove musiche: il suono
diviene storia del suono, storia del silenzio, storia della musica.

Nella meccanica del reale esperibile la coordinata temporale è inevitabile.
L'esperienza è nel tempo. Il tempo è esperienza. Se è presente la coordinata
temporale non può essereci silenzio. \emph{Il silenzio non esiste} novecentesco
è vero solo se ci si attiene alla meccanica tradizionale e al reale percepibile.
Tuttavia, il silenzio può esistere nelle strutture mentali e quindi nella fisica
quantistica, ovvero là dove la coordinata temporale smette di avere quel
significato. Potrei così giocare con il linguaggio: il silenzio è così
allusivamente attraente, relazionale, perché è quantistico. Il legame tra parola
e tempo ci pone nel dominio meccanico del dire, conducendo la parola, a volte
frammenti di essa, nel luogo senza tempo della relazione, si entra nel dominio
quantistico della poesia.

Tornando al sistema di riferimento musicale, il silenzio è nelle relazioni tra i
suoni, a cui tendiamo prima e dopo di ognuno di questi. La soglia che separa
l'uomo sensibile dal silenzio è una curva di ampiezze dipendenti dalla frequenza
nel tempo: un sistema musicale: in un sistema fisico: in un sistema filosofico.

\emph{E il rumore?} Il rumore è semplicemente la vibrazione di cui non si ha
ancora esperienza. La vita del rumore è la durata, la transizione, tra silenzio
ed esperienza.

Tendere a quella soglia ci porta a scoprire nuovi suoni, perfetti sconosciuti:
rumori. Siamo ascoltatori di uno spazio esteso oltre il conosciuto, come un
Galileo al tubo ottico: cerchiamo cose, per sentirne alcune, per udirne
altre, per dare nomi, per creare relazioni. Pionieri della \emph{sonazione}.

\begin{quote}
  [\ldots] l'unità di scienza e vita è appunto una unità attiva, in cui solo si
  realizza la libertà di pensiero, è un rapporto maestro-scolaro,
  filosofo-ambiente culturale in cui operare, da cui trarre i problemi necessari
  da impostare e risolvere, cioè il rapporto filosofia-storia. \cite{ag:matst}
\end{quote}% GRAMSCI P22

% $vibrazione(acustica) + esperienza(tempo) + spazio = timbro$. L'orecchio:
% strumento millenario, perfezionato per scappare e predare. Utilizzato come luogo
% creativo in cui costruire strumenti artificiali, l'orecchio, apre a nuove
% parole: Timbro. Quando predavamo scappando dai predatori non esisteva il
% timbro. Ci sono voluti secoli di strumenti artificiali, oggetti che hanno
% permesso una storia della musica, di tecnica e pensiero, per avere necessità di
% una nuova parola.
%
% Attività creativa, creazione come luogo particolareggiato del fare umano.
% Lo strumento come strumento di pensiero e la necessità di nuovi strumenti di
% pensiero.

L'allucinazione \emph{sonaria} è quindi una rappresentazione unica, ideale fino
a che non viene concessa e condivisa con una seconda persona: l'idea di un
singolo, privata agli altri, non è un oggetto sociale, mentre un'allucinazione
\emph{sonaria} condivisa e comprensibile è un oggetto sociale: è un possibile.
\cite{ferraris2014}
%-------------------------------------------------------------------------------
%----------------------------------------------------------------- SUB-SECTION -
%-------------------------------------------------------------------------------
\subsection{allucinazione e sogno}

Il motore che anima la ricerca è il sogno. Ogni scienza è guidata da un sogno.
Il sogno è l'anima della scienza, ogni scienza si distingue nella prospettiva di
un sogno: la ricerca di quel sogno specifico è lo spirito di quella scienza.
(Da Bergson) Il pensiero filosofico è tornato co-scienza della scienza. La
materia è memoria: la musica è un'interfaccia tecnologica della memoria.

\begin{quote}
  Non c’è, nella ragione del logos la linea che è creazione pura. La
  simbolizzazione della scrittura non può essere neppure trasformazione di una
  cosa percepita, ovvero immagine onirica, perché la linea, fuori ed oltre la
  mano dell’uomo che la segna, non esiste in natura e non può essere percepita
  e fatta ricordo o memoria. Noi percepiamo la linea che non ha figura ed ha
  forme infinite ed è senza identità manifesta, per la creazione della mano
  dell’uomo che fa i confini e definisce, rendendole visibili, le forme e la
  figura delle cose e delle immagini delle cose. E così crea anche l’identità
  della linea stessa che, ogni volta, è diversa perché infinitamente sottile o
  infinitamente lunga. E penso alla parola tempo che indica un movimento che
  non si ferma mai e non ha figura, né forma né confini. Ma il movimento
  invisibile della materia vivente non possiamo vederlo. Linea, movimento e
  tempo sono ancora tre parole che non indicano e non ricordano la figura
  di una cosa, ma sono... concetti, l’espressione di cose invisibili che si
  possono soltanto pensare. \cite{mf:left2008}
\end{quote}

La spiegazione del processo creativo che \emph{Cobb} condivide con
\emph{Ariadne}, del potenziale mentale nel sogno, è una delle tematiche
emergenti in \emph{Inception}, durante la cui scrittura l'autore, Christopher
Nolan, si è chiesto: «cosa accadrebbe se un gruppo di persone potesse
condividere (realmente) un sogno?».

\begin{figure}[ht]
\centering
%\resizebox{0.81\linewidth}{!}{%
\includegraphics[scale=1]{tikz/inception/inception.pdf}
\captionsetup{width=.81\linewidth}
\caption{Christopher Nolan, \emph{Inception} (2010).\\
         «Imagine you're designing a building. \textbf{You consciously create
         each aspect}. But sometimes,
         \textbf{it feels like it's almost creating itself}, if you know what I
         mean». «Yeah, like \textbf{I'm discovering it}». «Genuine inspiration,
         right? Now, in a dream, our mind continuously does this.
         \textbf{We create and perceive our world simultaneusly}. And our mind
         does this so well that we don't even know it's happening.
         \textbf{That allows us to get right in the middle of that process}».
         «How?». «\textbf{By taking over the creating part}».
         Il grafico disegnato da Cobb descrive il processo creativo di un sogno
         durante la costruzione di un edificio. Nel sogno, la curva che avanza
         da sinistra verso destra è il lavoro di costruzione che avanza mentre
         la curva che torna indietro da destra verso sinistra è la percezione,
         la scoperta di un mondo che si crea da solo. La riga centrale è «la
         presa in carico della parte creativa», la consapevolezza che si sta
         creando su due piani distinti.}
\label{tikz:inception}
\end{figure}

Il disegno in fig. \ref{tikz:inception} ricalca quello proposto da \emph{Cobb}
durante la spiegazione citata.
Osservandolo, mi chiedo: «non è quello che (simbolicamente) accade nella
condivisione di una ricerca?» L'attività creativa, quella che Fagioli definisce
creazione pura, non ha origini dal percepito, si attiva, è attività di
costruzione, di architettura, di autoprogettazione.

\begin{figure}[ht]
  \centering
  \includegraphics{tikz/ciclo-base/ciclobase.pdf}
  \captionsetup{width=.81\linewidth}
  \caption{ciclo base: l'invenzione è il trovato; la ricerca è coscienza protesa
  fuori dal tempo. L'atto creativo è un momento di ebaborazione della memoria che
  modifica l'ambiente.}
  \label{tikz:ciclobase}
\end{figure}

Il processo è trasducibile completamente nel dominio della creazione musicale,
la consapevolezza creativa, l'atto responsabile è l'asse su cui l'invenzione
avanza verso destra, nel dominio del tempo, mentre la ricerca si muove fuori
dal tempo, nello spazio mentale di relazione con la storia e l'ambiente.
Il disegno è simbolico del puro processo creativo, sogno o arte.
%
\marginpar{%
  \includegraphics[width=\marginparwidth]{images/apf.png}
  \captionof{figure}{\raggedright Diagramma di un filtro \emph{all-pass}.}
  \label{img:apf}
}
%
Osserviamo ora il diagramma della creazione: è un ciclo base tra l'invenzione,
il trovato nella relazione con l'ambiente e la pulsione di ricerca, con le sue
possibilità fuori del tempo e che modificano l'ambiente ri-alimentandolo con un
apparato di idee e materia completamente nuovi. Di fatto, se contempliamo il
processo creativo come quel flusso temporale in cui ci si può perdere un tempo
di sedimentazione, di accumulazione (come una pausa caffè e sigaretta) il
modello descritto è astraibile ad un filtro \emph{all-pass}. Nella letteratura
classica il filtro \emph{all-pass} viene descritto come unità riverberante. Il
processo creativo di relazioni intime, private, in una rete di relazioni tra
processi creativi può essere descritto come riverbero complesso, assimilabile
ad una \emph{Feedback Delay Network}.

%-------------------------------------------------------------------------------
%----------------------------------------------------------------- SUB-SECTION -
%-------------------------------------------------------------------------------
\subsection{sogno e politica}

\begin{quote}
  Che cos'è l'uomo? %È questa la domanda prima e princicpale della filosofia.
  [\ldots] Diaciamo dunque che l'uomo è un processo e precisamente è il
  processo dei suoi atti. [\ldots] occorre concepire l'uomo come una serie di
  rapporti attivi (un processo) in cui se l'individualità ha la massima
  importanza, non è però il solo elemento da considerare. L'umanità che si
  riflette in ogni individualità è composta di diversi elementi: 1) individuo;
  2) gli altri uomini; 3) la natura. [\ldots] Così l'uomo non entra in rapporti
  con la natura semplicemente per il fatto di essere egli stesso natura, ma
  attivamente, per mezzo del lavoro e della tecnica [\ldots] per tecnica deve
  intendersi [\ldots] anche gli strumenti «mentali», la conoscenza filosofica.
\end{quote}% GRAMSCI P27-28

È questo, innanzi tutto, un problema di linguaggio. Con linguaggio identifichiamo
un nome collettivo, che non presupone una cosa unica, è un \emph{oggetto sociale}.
\cite{ag:matst, ferraris2014} Esiste nella cultura, nella filosofia e nella
scienza, sia pure nel grado di senso comune e, in quanto tale, è una
molteplicità di oggetti, fatti, più o meno organicamente concreti e coordinati:

\begin{quote}
  al limite si può dire che ogni essere parlante ha un proprio linguaggio
  personale, cioè un proprio modo di pensare e sentire. \cite{ag:matst}
\end{quote}

E quindi la cultura unifica, raggruppa, cataloga a sua volta una maggiore o
minore quantità di individui più o meno a contatto espressivo, capaci di gradi
diversi di comprensione. Le differenze si riflettono, si riverberano nella
cultura, nel linguaggio comune e producono «ostacoli» e quelle «cause di errore»
che portano, da un lato all'incomprensione, dall'altro alla separazione, in
ogni caso riverbera in un problema ambientale. La conoscenza non è separata e
sufficiente a sé stessa, ma è coinvolta nella relazione individuo-ambiente e
nel processo culturale, vitale. I sensi non appaiono come ingressi della
conoscenza ma partecipano come stimoli all’azione.

\begin{quote}
  \ldots ognuno cambia se stesso, si modifica, nella misura in cui cambia e
  modifica tutto il complesso di rapporti di cui egli è il centro di annodamento.
  In questo senso il filosofo reale è e non può essere altri che il politico,
  cioè l'uomo attivo che modifica l'ambiente, inteso per ambiente l'insieme dei
  rapporti di cui ogni singolo entra a far parte. %Se la propria individualità
  %è l'insieme di questi rapporti, farsi una personalità significa acquistare
  %coscienza di tali rapporti, modificare la propria personalità significa
  %modificare l'insieme di questi rapporti.
  [\ldots] Ma questi rapporti, come si è detto, non sono semplici. Intanto
  alcuni di essi sono necessari, altri volontari. Inoltre averne coscienza più
  o meno profonda (cioè conoscere più o meno il modo con cui si possono
  modificare) già li modifica. Gli stessi rapporti necessari in quanto
  conosciuti nella loro necessità, cambiano d'aspetto e d'importanza.
  La conoscenza è potere in questo senso. \cite{ag:matst}
\end{quote}% GRAMSCI P29

Se la musica ha avuto ed ha un ruolo culturale fuori dall'intrattenimento, è
in questi \emph{non}-spazi e in questi \emph{non}-tempi sociali. Se il musicista
ha ancora un ruolo sociale fuori dall'intrattenitore è in questi spazi e in
questi tempi culturali. Ridefinire il proprio ambito di intervento culturale e
sociale implica una concertazione, un accordo in maniera che la meta della
ricerca sia il risultato di un’appropriata combinazione di accordo non
costrittivo e tollerante disaccordo.

\begin{quote}
  Secoli e secoli di idealismo non hanno mancato di influire sulla realtà. \cite{jlb:finzioni}
\end{quote}% BORGES

\clearpage
