\version "2.24.3"

#(set-global-staff-size 18)
\header {tagline = ""}
%
\paper {
  indent = 0\mm
  paper-width = 15\cm
  paper-height = 55\mm
  left-margin = 15\mm
  %top-margin = 5\mm
  %bottom-margin = 5\mm
  print-page-number = ##f

  #(define fonts
    (set-global-fonts
      #:music "haydn"
      #:brace "haydn"
      #:roman "Alegreya"
      #:sans "sans-serif"
      #:typewriter "monospace"
  ))
  #(include-special-characters)
}

ringsps = "
  0.15 setlinewidth
  0.9 0.6 moveto
  0.4 0.6 0.5 0 361 arc
  stroke
  1.0 0.6 0.5 0 361 arc
  stroke
  "

rings = \markup {
  \with-dimensions #'(-0.2 . 1.6) #'(0 . 1.2)
  \postscript #ringsps
}

\layout {
  \context {
    \Score
    \accidentalStyle forget
    \remove "Bar_number_engraver"
    \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/128)
    \override NoteHead.font-size = #-2
  }
}

<<
\new Staff
  \new Voice \with {
    \consists "Ambitus_engraver"
  } \relative c {
    \override Score.TimeSignature.stencil = ##f
    \override Score.Stem.stencil = ##f
    \override Score.Rest.stencil = ##f
    \clef bass
    %\override Ambitus.X-offset = 3.0
    \voiceOne
    %\magnifyMusic 0.81 {
    %d,1^\markup{-900} \glissando g \glissando \once \normalsize cis \glissando f\flageolet^\markup{nodale}
    %}
    gis1^\markup{\small \raise #1 \center-align accordatura}
    gis2_\markup{\small \lower #3.5 ventrale} \glissando d \glissando
    b2_\markup{\small \lower #3.5 scordatura}_\markup{\teeny \lower #3.5 \center-align -900°} \glissando a'_\markup{\teeny \lower #1.5 \center-align +450°}
    %
    fis'2^\rings \glissando fis'^\rings
   }

\new Staff
  \new Voice \with {
    \consists "Ambitus_engraver"
  } \relative c {
    \override Score.TimeSignature.stencil = ##f
    \override Score.Stem.stencil = ##f
    \override Score.Rest.stencil = ##f
    \clef bass
    \voiceTwo
    r1
    d2 \glissando aes \glissando
    e2 \glissando d'
    %
    g2^\rings_\markup{\small \lower #3 nodale}_\markup{\teeny \lower #3 \center-align -900°} \glissando g'^\rings_\markup{\teeny \center-align +450°}
    % \magnifyMusic 0.81 {
    % b,1 \glissando d \glissando gis \glissando c
    % }
  }
>>
