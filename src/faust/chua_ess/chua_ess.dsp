import("stdfaust.lib");
// -----------------------------------------------------
// VARIABILI
dur = 19*ma.SR;
// sax parte un'ottava sotto e si aggancia un'ottava dopo
sfmin = ba.midikey2hz(14); //  18.35 Hz D-1
sfmax = ba.midikey2hz(81); // 880.00 Hz A4
// voce parte tre ottave sotto
vfmin = ba.midikey2hz(14); //  18.35 Hz D-1
vfmax = ba.midikey2hz(50); // 146.83 Hz D2
efmin = ba.midikey2hz(22); //  29.14 Hz Bb-1
efmax = ba.midikey2hz(77); // 698.46 Hz F4
tfmin = ba.midikey2hz(26); //  36.71 Hz D0
tfmax = ba.midikey2hz(50); // 146.83 Hz D2

// -----------------------------------------------------
// DCBLOCK
// dcblock(igain) = _ <: _ - mem: +~*(igain);
dcblock = _ <: (_- seq(i,4,eavg(acor(5))))///sqrt(2)
  with{
      cosq(x) = cos(x)*cos(x);
      twopi = 2*ma.PI;
      omega(fc) = fc*twopi/ma.SR;
      acor(fc) = cos(omega(fc))-1+sqrt(cosq(omega(fc))-4*cos(omega(fc))+3);
      eavg(a) = *(a) : +~*(1-a);
  };

// -----------------------------------------------------
// FUNZIONI DI SWEEP
// sweep lineare numerico di faust corretto 0~m
sweep(m,t) = m+1 : %(int(*(t):max(1)))~+(1');

// sweep esponenziale numerico
esweep(fmin,fmax,dur,e) = sweep(dur,1)*(1/dur)*(fmax-fmin) : ^(e)/((fmax+fmin)^(e-1)) : +(fmin);

// sweep esponenziale numerico con display frequenze
esweepd(fmin,fmax,dur,e) = sweep(dur,1)*(1/dur)*(fmax-fmin) : ^(e)/((fmax+fmin)^(e-1)) : +(fmin) <:
                                attach(_,hbargraph("",ba.midikey2hz(10),ba.midikey2hz(82)));

// -----------------------------------------------------
// ESS
// exponential sine sweep con attennuazione 1/f e filtro dcblock
ess(fmin,fmax,dur,e) = os.osc(esweepd(fmin,fmax,dur,e)) : no.pink_filter : dcblock;

process = ess(sfmin,sfmax,dur,1),
          ess(vfmin,vfmax,dur,1),
          ess(efmin,efmax,dur,1),
          ess(tfmin,tfmax,dur,1) :> _/2,_/2;
