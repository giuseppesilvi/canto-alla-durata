import("stdfaust.lib");
t = hslider("Smallest T",61,1,100,1);
fbk = hslider("FeedBack", 0.708, 0, 0.999, 0.001) : si.smoo;
// nextprime ffunction
np = ffunction(int next_pr(int), "nextprime.h","");
// comb
//dflc(t,g) = + @(t-1) ~ *(g) : mem;
dflc(t,g) = (+ : de.delay(4*ma.SR, t-1))~*(g) : mem;
// all-pass
apf(t,g) = _ <: *(-g) + (dflc(t,g) : *(1-(g*g)));

// golden ratio
phi(x) = x * (1+(sqrt(5)))/2;
phit(N,t) = par(i,N,phi(t*i):np);
//process = phit(4,61);
//process = apf(phi(61):np,0.707);

// phi-tuned all-pass reverb
inphirev(N,t,g) = seq(i,N,apf(t*(i+1):np,g));
process = si.bus(4) <: par(i,16,inphirev(81,100*(i+1),0.707));
